export default {
  'SECRET_APP_KEY': 'HJOGJIOSUG89U',
  'PORTS': {
    'NODE': 3000,
    'MONGOOSE': 27017
  },
  'ROUTES' : {
    'LOGIN': '/login',
    'REGISTRATION': '/registration',
    'LOGOUT': '/logout',
    'FACEBOOK': '/facebook',
    'POST': '/post',
    'POSTS': '/getposts',
    'UPLOAD': '/upload',
    'DOWNLOAD': '/download',
    'FILE': '/file',
    'PICTURE': '/picture'
  },
  'CONTROLLER': {
    'FILE': '/file'
  },
  'UPLOAD_DIR': '/data',
  'USER_MODEL': {
    '_ID': '_id',
    'USER_ID': 'user_id',
    'EMAIL': 'email',
    'PASSWORD': 'password',
    'FACEBOOK': 'facebook',
    'F_TOKEN': 'f_token'
  },
  'TOKEN_MODEL': {
    'USER_ID': 'user_id',
    'TOKEN': 'token'
  },
  'POST_MODEL' : {
    'USER_ID': 'user_id',
    'TITLE': 'title',
    'DESCRIPTION': 'description'
  },
  'FILE_MODEL': {
    'ID':'_id',
    'FILENAME': 'filename'
  },
  'BODY_PARAM': {
    'MESSAGE': 'message',
    'USER': 'user',
    'EMAIL': 'email',
    'FACEBOOK': 'facebook',
    'F_TOKEN': 'f_token',
    'PASSWORD': 'password',
    'PICTURE': 'picture',
    'UID': 'uid',
    'TOKEN': 'token',
    'SECRET': 'secret',
    'DESCRIPTION': 'description',
    'TITLE': 'title',
    'ID': 'id',
    'POSTS': 'posts'
  },
  'STATIC_FIELD': {
    'CREATED_AT': 'created_at',
    'UPDATED_AT': 'updated_at'
  }
}