import CryptoJS from 'crypto-js';
import Constants from './constants';
import Token from '../models/token';

export default {
    encryptHash(pass) {
        return CryptoJS.MD5(pass).toString();
    },

    createOrGetToken(searchObj) {
        return new Promise((res,rej) => {
            Token.TokensModel.findOne({ [Constants.TOKEN_MODEL.USER_ID]: searchObj[Constants.TOKEN_MODEL.USER_ID] }, (err, token) => {
                    if(err) {
                        rej(err);
                        return;
                    }
                    if(null == token) {
                        let newToken = this.encryptHash(Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5));
                        let tokenUser = new Token.TokensModel({
                            [Constants.TOKEN_MODEL.USER_ID]: searchObj[Constants.USER_MODEL.USER_ID],
                            [Constants.TOKEN_MODEL.TOKEN]: newToken
                        });
                        tokenUser.save((err) => { });
                        res(newToken);
                    } else {
                        res(token[Constants.TOKEN_MODEL.TOKEN]);
                    }
                }
            );
        });
    }
}