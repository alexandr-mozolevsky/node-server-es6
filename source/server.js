import express from 'express';
import bodyParser from 'body-parser';
import Constants from './common/constants';
import assert from 'assert';

import MongooseHelper from './controllers/mongoose';
import FileController from './controllers/file';
import AuthController from './controllers/auth';
import PostController from './controllers/post';

var app = express();
var jsonParser = bodyParser.json();

app.use(jsonParser);
app.use(express.static(__dirname + '/data'));
//app.use((req, res, next) => {
//    res.setHeader('Access-Control-Allow-Origin', '*');
//    res.setHeader('Accept', 'application/json');
//    res.setHeader('Content-Type', 'application/json');
//    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
//    next();
//});

class Server {
    constructor() {
        console.log('Created Server');
        app.listen(Constants.PORTS.NODE, () => {
            console.log(`Port listening on ${ Constants.PORTS.NODE }`);
        });

        MongooseHelper.initMongoose();

        this.registerRouters();
    }

    registerRouters() {
        app.post(Constants.ROUTES.REGISTRATION, (req, res) => {
            AuthController.registerUser({
                [Constants.BODY_PARAM.EMAIL] : req.body[Constants.BODY_PARAM.EMAIL],
                [Constants.BODY_PARAM.PASSWORD]: req.body[Constants.BODY_PARAM.PASSWORD]
            }, res);
        });

        app.post(Constants.ROUTES.LOGIN, (req, res) => {
            AuthController.loginUser({
                [Constants.BODY_PARAM.EMAIL]: req.body[Constants.BODY_PARAM.EMAIL],
                [Constants.USER_MODEL.PASSWORD]: req.body[Constants.BODY_PARAM.PASSWORD]
            }, res);
        });

        app.get(Constants.ROUTES.LOGOUT, (req, res) => {
            AuthController.logoutUser({
                [Constants.BODY_PARAM.TOKEN]: req.get(Constants.BODY_PARAM.TOKEN)
            }, res);
        });

        app.post(Constants.ROUTES.FACEBOOK, (req, res) => {
            AuthController.loginFacebook({
                [Constants.BODY_PARAM.UID]: req.body[Constants.BODY_PARAM.UID],
                [Constants.BODY_PARAM.F_TOKEN]: req.body[Constants.BODY_PARAM.F_TOKEN],
                [Constants.BODY_PARAM.SECRET]: req.body[Constants.BODY_PARAM.SECRET]
            }, res);
        });

        app.post(Constants.ROUTES.POST, (req, res) => {
            PostController.addPost({
                [Constants.BODY_PARAM.TOKEN]: req.get(Constants.BODY_PARAM.TOKEN),
                [Constants.BODY_PARAM.TITLE]: req.body[Constants.BODY_PARAM.TITLE],
                [Constants.BODY_PARAM.DESCRIPTION]: req.body[Constants.BODY_PARAM.DESCRIPTION]
            }, res);
        });

        app.get(Constants.ROUTES.POST, (req, res) => {
            PostController.getPosts({
                [Constants.BODY_PARAM.TOKEN]: req.get(Constants.BODY_PARAM.TOKEN)
            }, res);
        });

        app.get(Constants.CONTROLLER.FILE + Constants.ROUTES.DOWNLOAD, (req, res) => {
            FileController.getFiles(res);
        });

        app.post(Constants.CONTROLLER.FILE + Constants.ROUTES.UPLOAD, (req, res) => {
            FileController.uploadFile(req, res, __dirname + Constants.UPLOAD_DIR);
        });

        app.get(`${Constants.CONTROLLER.FILE}${Constants.ROUTES.DOWNLOAD}/:id`, jsonParser, (req, res) => {
            FileController.downloadFile({
                [Constants.FILE_MODEL.ID]: req.params.id
            }, res);
        });

        app.get(`${Constants.ROUTES.PICTURE}/:id`, (req, res) => {
            FileController.getPicture({
                [Constants.BODY_PARAM.PICTURE]: req.params.id
            }, [ res, __dirname ]);
        });
    }
}

var _Server = new Server();
export default _Server;