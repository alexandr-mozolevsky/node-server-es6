import { Schema } from 'mongoose';
import Constants from '../common/constants';

let Users = new Schema({
    [Constants.USER_MODEL.USER_ID]: { type: String },
    [Constants.USER_MODEL.EMAIL]: { type: String, required: false },
    [Constants.USER_MODEL.PASSWORD]: { type: String, required: false },
    [Constants.USER_MODEL.FACEBOOK]: { type: String, required: false },
    [Constants.USER_MODEL.F_TOKEN]: { type: String, required: false }
},{
    versionKey: false,
    timestamps: {
        createdAt: Constants.STATIC_FIELD.CREATED_AT,
        updatedAt: Constants.STATIC_FIELD.UPDATED_AT
    }
});

Users.path([Constants.USER_MODEL.EMAIL]).validate(v => {
    return v.length > 5 && v.length < 70;
}, 'Email field must be from 5 to 70 length');

Users.path([Constants.USER_MODEL.EMAIL]).validate((value, respond) => {
    _User.UsersModel.findOne({ username: value }, (err, user) => {
        if(err) throw err;
        if(user) return respond(false);
        respond(true);
    });
}, 'User with this username is exist');

class User {
    init(database) {
        this.database = database;
        this.UsersModel = this.database.model('Users', Users);
    }
}

var _User = new User();
export default _User;
