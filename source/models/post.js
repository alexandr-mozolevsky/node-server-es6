import Mongoose, { Schema } from 'mongoose';
import Constants from '../common/constants';

let Posts = new Schema({
    [Constants.POST_MODEL.USER_ID]: { type: String, required: true },
    [Constants.POST_MODEL.TITLE]: { type: String, required: true },
    [Constants.POST_MODEL.DESCRIPTION]: { type: String, required: true }
},{
    versionKey: false,
    timestamps: {
        createdAt: Constants.STATIC_FIELD.CREATED_AT,
        updatedAt: Constants.STATIC_FIELD.UPDATED_AT
    }
});

Posts.path([Constants.POST_MODEL.TITLE]).validate(v => {
    return v.length > 5 && v.length < 70;
}, 'Post title field must be from 5 to 70 length');

Posts.path([Constants.POST_MODEL.DESCRIPTION]).validate(v => {
    return v.length > 5 && v.length < 70;
}, 'Post description field must be from 5 to 70 length');

class Post {
    init(database) {
        this.database = database;
        this.PostsModel = this.database.model("Posts", Posts);
    }
}

var _Post = new Post();
export default _Post;