import Mongoose, { Schema } from 'mongoose';
import Constants from '../common/constants';

let Tokens = new Schema({
    [Constants.TOKEN_MODEL.USER_ID]: { type: String, required: true },
    [Constants.TOKEN_MODEL.TOKEN]: { type: String, required: true }
}, {
    versionKey: false
});

class Token {
    init(database) {
        this.database = database;
        this.TokensModel = this.database.model('Tokens', Tokens);
    }
}

var _Token = new Token();
export default _Token;
