import Mongoose from 'mongoose';

import User from '../models/user';
import Token from '../models/token';
import Post from '../models/post';
import File from '../controllers/file';
import Constants from '../common/constants';
import Common from '../common/common';

class MongooseHelper {
    database;

    initMongoose() {
        this.database = Mongoose.createConnection(`mongodb://localhost:${ Constants.PORTS.MONGOOSE }/test`, (err, db) => {
            if(!err) {
                console.log(`MongoDb correctly connected to ${ Constants.PORTS.MONGOOSE }`);
            } else {
                console.log(err);
                if(db != null)
                    db.close();
            }
        });
        User.init(this.database);
        Token.init(this.database);
        Post.init(this.database);
        File.init(this.database);
    }
}

var _MHelper = new MongooseHelper();
export default _MHelper;