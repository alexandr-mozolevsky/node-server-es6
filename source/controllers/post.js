import Mongoose from 'mongoose';

import User from '../models/user';
import Token from '../models/token';
import Post from '../models/post';
import Constants from '../common/constants';
import Common from '../common/common';

class PostHelper {
    addPost(params, response) {
        Token.TokensModel.findOne({ [Constants.TOKEN_MODEL.TOKEN]: params[Constants.TOKEN_MODEL.TOKEN] }, (err, token) => {
            if(err || token == null)
                return response.status(500).send({ [Constants.BODY_PARAM.MESSAGE]: 'server error' });

            User.UsersModel.findOne({ [Constants.USER_MODEL._ID]: token[Constants.TOKEN_MODEL.USER_ID] }, (err, user) => {
                let currentPost = new Post.PostsModel({
                    [Constants.POST_MODEL.USER_ID]: user[Constants.USER_MODEL._ID],
                    [Constants.POST_MODEL.TITLE]: params[Constants.POST_MODEL.TITLE],
                    [Constants.POST_MODEL.DESCRIPTION]: params[Constants.POST_MODEL.DESCRIPTION]
                });

                currentPost.save( err => {
                    if(!err)
                        response.status(200).send({ [Constants.BODY_PARAM.MESSAGE]: 'post added' });
                    else
                        response.status(500).send({ [Constants.BODY_PARAM.MESSAGE]: err });
                });
            });
        });
    }

    getPosts(params, response) {
        Token.TokensModel.findOne({[Constants.TOKEN_MODEL.TOKEN]: params[Constants.TOKEN_MODEL.TOKEN]}, (err, token) => {
            if (err || token == null)
                return response.status(500).send({[Constants.BODY_PARAM.MESSAGE]: 'server error'});

            Post.PostsModel.find({[Constants.POST_MODEL.USER_ID]: token[Constants.TOKEN_MODEL.USER_ID]}, (err, posts) => {
                console.log(posts);
                return response.status(200).send({
                    [Constants.BODY_PARAM.MESSAGE]: 'success',
                    [Constants.BODY_PARAM.POSTS]: posts
                });
            });
        });
    }
}

var _PostHelper = new PostHelper();
export default _PostHelper;