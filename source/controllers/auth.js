import User from '../models/user';
import Token from '../models/token';
import Post from '../models/post';
import Constants from '../common/constants';
import Common from '../common/common';

class AuthHelper {
    registerUser(user, response) {
        let currentUser = new User.UsersModel({
            [Constants.USER_MODEL.EMAIL]: user[Constants.BODY_PARAM.EMAIL],
            [Constants.USER_MODEL.PASSWORD]: Common.encryptHash(user[Constants.BODY_PARAM.PASSWORD])
        });

        User.UsersModel.findOne({ [Constants.USER_MODEL.EMAIL]: currentUser[Constants.USER_MODEL.EMAIL] }, (err, obj) => {
            if(err)
                response.status(500).send({ [Constants.BODY_PARAM.MESSAGE]: 'server error' });

            if(null != obj) {
                response.status(400).send({ [Constants.BODY_PARAM.MESSAGE]: 'user exist with this email' });
            } else {
                currentUser.save((err, obj) => {
                    if (!err) {
                        Common.createOrGetToken({ [Constants.TOKEN_MODEL.USER_ID]: obj[Constants.USER_MODEL._ID] })
                            .then(result => {
                                return response.status(200).send({ [Constants.BODY_PARAM.MESSAGE]: 'you are logged in' , [Constants.BODY_PARAM.TOKEN]: result, [Constants.BODY_PARAM.USER]: currentUser });
                            });
                    } else {
                        if (err.name == 'ValidationError') {
                            response.status(400).send({ [Constants.BODY_PARAM.MESSAGE]: err.message });
                        } else {
                            response.status(500).send({ [Constants.BODY_PARAM.MESSAGE]: err.message });
                        }
                    }
                });
            }
        });
    }

    loginUser(user, response) {
        let currentUser = {
            [Constants.USER_MODEL.EMAIL]: user[Constants.BODY_PARAM.EMAIL],
            [Constants.USER_MODEL.PASSWORD]: user[Constants.BODY_PARAM.PASSWORD]
        };
        console.log(currentUser);
        User.UsersModel.findOne({ [Constants.USER_MODEL.EMAIL]: currentUser[Constants.USER_MODEL.EMAIL] }, (err, obj) => {
            if(err)
                response.status(500).send({ [Constants.BODY_PARAM.MESSAGE]: 'server error' });
            if(null == obj) {
                response.status(404).send({ [Constants.BODY_PARAM.MESSAGE]: 'user doesn\'t exist' });
            }
            if(obj.password != Common.encryptHash(currentUser[Constants.USER_MODEL.PASSWORD])) {
                response.status(403).send({ [Constants.BODY_PARAM.MESSAGE]: 'password doesn\'t match' });
            } else {
                Common.createOrGetToken({ [Constants.TOKEN_MODEL.USER_ID]: obj[Constants.USER_MODEL._ID] })
                    .then(result => {
                        response.status(200).send({ [Constants.BODY_PARAM.MESSAGE]: 'you are logged in' , [Constants.BODY_PARAM.TOKEN]: result });
                    });
            }
        });
    }

    logoutUser(token, response) {
        let currentToken = {
            [Constants.TOKEN_MODEL.TOKEN]: token[Constants.BODY_PARAM.TOKEN]
        };

        Token.TokensModel.findOne({ [Constants.TOKEN_MODEL.TOKEN]: currentToken[Constants.TOKEN_MODEL.TOKEN] }, (err, obj) => {
            if(err)
                response.status(500).send({ [Constants.BODY_PARAM.MESSAGE]: 'server error' });
            if(null == obj) {
                response.status(403).send({ [Constants.BODY_PARAM.MESSAGE]: 'user doesn\'t exist in token database' });
            }
            obj.remove((error) => {
                if(error)
                    response.status(400).send({ [Constants.BODY_PARAM.MESSAGE]: error.message });
                response.status(200).send({ [Constants.BODY_PARAM.MESSAGE]: 'user logouted' });
            });
        });
    }

    loginFacebook(params, response) {
        let currentFacebook = new User.UsersModel({
            [Constants.USER_MODEL.FACEBOOK]: params[Constants.BODY_PARAM.UID],
            [Constants.USER_MODEL.F_TOKEN]: params[Constants.BODY_PARAM.F_TOKEN]
        });

        if([Constants.SECRET_APP_KEY] != params[Constants.BODY_PARAM.SECRET]) {
            return response.status(403).send({ [Constants.BODY_PARAM.MESSAGE]: 'secret password does\'t match' });
        }
        User.UsersModel.findOne({ [Constants.USER_MODEL.FACEBOOK]: params[Constants.USER_MODEL.FACEBOOK] }, (err, obj) => {
            if(err)
                response.status(500).send({ [Constants.BODY_PARAM.MESSAGE]: 'server error' });

            if(obj == null) {
                currentFacebook.save((err, currentObj) => {
                    if(err)
                        response.status(500).send({ [Constants.BODY_PARAM.MESSAGE]: 'server error' });

                    Common.createOrGetToken({ [Constants.TOKEN_MODEL.USER_ID]: currentObj[Constants.USER_MODEL._ID] })
                        .then(result => {
                            response.status(200).send({ [Constants.BODY_PARAM.MESSAGE]: 'facebook logged in' , [Constants.BODY_PARAM.TOKEN]: result, [Constants.BODY_PARAM.USER]: obj});
                        });
                });
            } else {
                Common.createOrGetToken({ [Constants.TOKEN_MODEL.USER_ID]: obj[Constants.USER_MODEL._ID] })
                    .then(result => {
                        response.status(200).send({ [Constants.BODY_PARAM.MESSAGE]: 'facebook logged in' , [Constants.BODY_PARAM.TOKEN]: result, [Constants.BODY_PARAM.USER]: obj});
                    });
            }
        });
    }
}

var _AuthHelper = new AuthHelper();
export default _AuthHelper;