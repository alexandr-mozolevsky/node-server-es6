import Mongoose from 'mongoose';
import Constants from '../common/constants';
import Common from '../common/common';

import Formidable from 'formidable';
import GridFS from 'gridfs-stream';
import FS from 'fs';

class FileHelper {
    init(database) {
        this.database = database;
    }

    uploadFile(request, response, uploadDir) {
        let form = new Formidable.IncomingForm();
        form.uploadDir = uploadDir;
        form.keepExtensions = true;
        form.parse(request, (err, fields, files) => {
            if(typeof files.file == 'undefined') {
                return response.status(400).send({ message: 'Unable to catch file' });
            }
            if(!err) {
                GridFS.mongo = Mongoose.mongo;
                let GFS = GridFS(this.database.db);
                let writestream = GFS.createWriteStream({
                    [Constants.FILE_MODEL.FILENAME]: files.file.name
                });
                FS.createReadStream(files.file.path).pipe(writestream);
            } else {
                console.log(err);
            }
        });
        form.on('end', () => {
            response.send('Completed ..... go and check fs.files & fs.chunks in  mongodb');
        });
    }

    getFiles(res) {
        GridFS.mongo = Mongoose.mongo;
        let GFS = GridFS(this.database.db);
        GFS.files.find({}).toArray((err, files) => {
            let a_f = files.map(file => (
                { id: file._id, filename: file.filename }
            ));

            res.send(a_f);
        });
    }

    downloadFile(data, response) {
        GridFS.mongo = Mongoose.mongo;
        let GFS = GridFS(this.database.db);
        GFS.findOne({ [Constants.FILE_MODEL.ID]: data[Constants.FILE_MODEL.ID]}, (err, obj) => {
            if (err)
                response.status(400).send(err);
            if (!obj)
                response.status(404).send('File is empty');

            response.set('Content-Type', obj.contentType);
            response.set('Content-Disposition', 'attachment; filename="' + obj.filename + '"');

            var readstream = GFS.createReadStream({
                _id: obj._id
            });

            readstream.on('error', (error) => {
                console.log('Got error while processing stream ' + error.message);
                response.end();
            });

            readstream.pipe(response);
        });
    }

    getPicture(data, dest) {
        let [response, dir] = dest;
        var options = {
            root: `${dir}${Constants.UPLOAD_DIR}/`,
            dotfiles: 'deny',
            headers: {
                'x-timestamp': Date.now(),
                'x-sent': true,
                'Content-Type': 'image/png'
            }
        };
        response.sendFile(`${data[Constants.BODY_PARAM.PICTURE]}`, options);
    }
}

var _FileHelper = new FileHelper();
export default _FileHelper;